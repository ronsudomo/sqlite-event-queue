const EventQueue = require('./event-queue')

function generateFakeEvents(eventQueue, opts = { max: 10, delayMillis: 1000 }) {
    let i = 0
    const taskId = setInterval(async () => {
        if (++i <= opts.max) {
            const evtId = `${Date.now()}.${i}`
            await eventQueue.pushEvent({ "id": evtId, "name": `evt-${evtId}` })
        } else {
            clearInterval(taskId)
        }
    }, opts.delaySecs)
}

function randomFailure(probability = 0.5) {
    return !!probability && Math.random() <= probability;
}

//--- MAIN SCRIPT ---

const q = new EventQueue()

generateFakeEvents(q, { max: 100, delayMillis: 100 })

q.processEvents(
    evt => {
        console.log(`PROCESSING-EVENT: ${JSON.stringify(evt)}`)

        // Simulate random failure to test if failed event processing will be retried
        const failed = randomFailure(0.2)
        if (failed) {
            console.log(`--FAILED--`)
        }
        return !failed
    },
    { batch: 5, delayMillis: 1000 }
)