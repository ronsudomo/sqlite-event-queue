const sqlite3 = require('sqlite3')
const Promise = require('promise')
const fs = require('fs')

class EventQueue {

    constructor(opts = {}) {
        const defaultOpts = {
            dbFile: 'event-queue.db',
            dbVacuum: {
                delayMins: 60,
                maxFileSizeMB: 10
            }
        }
        this.opts = { ...defaultOpts, ...opts }

        this.db = new sqlite3.Database(this.opts.dbFile, (err) => {
            if (err) {
                return console.error('Could not connect to database', err)
            } else {
                console.log('Connected to database')
            }
        })

        if (!fs.existsSync(this.opts.dbFile)) {
            this._initDb()
        }

        if (this.opts.dbVacuum) {
            this._runDbVacuumPeriodically(this.opts.dbVacuum)
        }
    }

    _initDb() {
        this.db.serialize(() => {
            this.db.run(`
                CREATE TABLE event (
                id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                json_data TEXT,
                timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL)`)

            this.db.run(`CREATE INDEX idx_event_timestamp ON event (timestamp)`)
        })
    }

    /**
     * In SQLite database, insert, update, and delete operations will usually cause 
     * the DB file to grow in size and data will also become fragmented. 
     * For more info, see https://www.techonthenet.com/sqlite/vacuum.php
     * 
     * Therefore, we should run VACUUM periodically, to ensure that DB operations
     * will not degrade over time.
     * 
     * NOTE:
     *   Another option is to set auto_vacuum = 1 before creating tables,
     *   however auto_vacuum does not perform defragmentation. This means that
     *   tables & indexes may still be stored inefficiently.
     */
    _runDbVacuumPeriodically(opts) {
        setInterval(() => {
            const fileSizeMB = fs.statSync(this.opts.dbFile).size / 1000000
            if (fileSizeMB >= opts.maxFileSizeMB) {
                this.db.run(`VACUUM`)
            }
        }, opts.delayMins * 60 * 1000)
    }

    pushEvent(jsonData) {
        const sql = `INSERT INTO event (json_data) values (?)`
        return this._run(sql, [JSON.stringify(jsonData)])
    }

    getNextEvents(limit = 1) {
        const sql = `SELECT * FROM event ORDER BY timestamp LIMIT ?`
        return this._all(sql, [limit])
    }

    clearEvents(ids) {
        const placeholders = Array(ids.length).fill('?').join(',')
        const sql = `DELETE FROM event WHERE id in (${placeholders})`
        return this._run(sql, ids)
    }

    processEvents(procFunc, opts = { batch: 3, delayMillis: 1000 }) {
        setInterval(async () => {
            let successfullyProcessed = []

            try {
                const events = await this.getNextEvents(opts.batch)

                for (let i = 0; i < events.length; i++) {
                    const jsonData = JSON.parse(events[i].json_data)

                    const success = await procFunc(jsonData)
                    if (success) {
                        successfullyProcessed.push(events[i].id)
                    } else {
                        // Since events must be processed sequentially,
                        // we should stop processing events once an error is encountered,
                        // but keep the event in the queue, so it can be retried in the
                        // next interval
                        break
                    }
                }
            } catch (err) {
                console.error('Error while processing events', err)
            }

            // Clear events that have been successfully processed
            if (successfullyProcessed.length > 0) {
                await this.clearEvents(successfullyProcessed)
            }
            // console.debug('...')
        }, opts.delayMillis)
    }

    _run(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.db.run(sql, params, function (err) {
                if (err) {
                    console.error(`Error running SQL: ${sql}`, err)
                    reject(err)
                } else {
                    resolve(true)
                }
            })
        })
    }

    _all(sql, params = []) {
        return new Promise((resolve, reject) => {
            this.db.all(sql, params, (err, rows) => {
                if (err) {
                    console.error(`Error running SQL: ${sql}`, err)
                    reject(err)
                } else {
                    resolve(rows)
                }
            })
        })
    }
}

module.exports = EventQueue
